﻿using System;

namespace simple_while
{
    class Program
    {
        static void Main(string[] args)
        {
            var n = 1;
            var m = 1;
            while (n <= 5){
                Console.WriteLine($"El numero consecutivo es: {n} soy el while");
                n++;// n = n + 1;
                m = 1;
                do
                {
                    Console.WriteLine($"El numero consecutivo es: {m} soy el do");
                    m++;// n = n + 1; 
                } while (m <= 5);
            }
            
        }
    }
}
