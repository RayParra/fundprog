﻿using System;

namespace arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            var lon = 3;
            int[] x = new int[lon];
            for (int i = 0; i < x.Length; i++)
            {
                Console.WriteLine($"Escribe el Valor {i+1} de {lon}: ");
                x[i] = Int32.Parse(Console.ReadLine());
            }
           for (int i = 0; i < x.Length; i++){
               Console.WriteLine(x[i]);
           }
           Console.WriteLine($"La Longitud del arreglo es: {x.Length}");
        }
    }
}
