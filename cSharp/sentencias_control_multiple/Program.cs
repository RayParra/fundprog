﻿using System;

namespace sentencias_control_multiple
{
    class Program
    {
        static void Main(string[] args)
        {
            var dia = 30;
            var mes = 90;
            var year = 2021;

            if (dia == 30){
                Console.WriteLine("El dia del vencimiento es correcto");
                if (mes == 9){
                    Console.WriteLine("El Mes del vencimiento es correcto. ");
                    if(year == 2021){
                        Console.WriteLine("El Año del vencimiento es correcto");
                 }
                    } 
            } 
            else {
                Console.WriteLine("El dia o el Mes o el Año de vencimiento no es el correcto");
            }

            //Console.WriteLine("Hello World!");
        }
    }
}
