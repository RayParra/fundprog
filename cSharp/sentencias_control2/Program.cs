﻿using System;

namespace sentencias_control2
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Se establece que los puertos del 3000 al 6000 son puertos
                utilizados por servidores de base de datos y los puertos del 
                6001 al 9000 son puertos utilizados por los servidores WEB, crear
                un programa para solicitar un numero de puerto al usuario y 
                determinar si es un puerto para servidor de base de datos o 
                para un servidor WEB
            */

            int num_puerto;

            Console.WriteLine("Escribe el numero de Puerto: ");
            num_puerto = Int32.Parse(Console.ReadLine());

            if ((num_puerto >= 3000) && (num_puerto <= 6000))
            {
                Console.WriteLine("El puerto mencionado es para un servidor de Base de datos");
            }
            else if ((num_puerto >= 6001) && (num_puerto <= 9000)){
                Console.WriteLine("El puerto mencionado es para un servidor WEB");
            }
            else {
                Console.WriteLine("El puerto mencionado es para uso comun");
            }
        }
    }
}
