﻿using System;

namespace registros
{
    public struct Customer
    {
       public int id;
       public string name;
       public string email;
       public bool is_active;

       public Customer(int newid, string newname, string newemail){
           id = newid;
           name = newname;
           email = newemail;
           is_active = true;
       }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Customer c = new Customer(1001, "Foo", "foo@bar.com");
            var c2 = new Customer(1002, "Bar", "bar@foo.com");
            Console.WriteLine($"El Id del customer {c.name} es: {c.id} y su email es: {c.email}");
            Console.WriteLine("Escribe el nuevo nombre para el customer:  ");
            c.name = Console.ReadLine();
            Console.WriteLine($"El nuevo nombre del customer es: {c.name} y su estatus es: {c.is_active}");
            Console.WriteLine($"Bienvenido {c2.name} eres un nuevo cliente");
        }
    }
}
