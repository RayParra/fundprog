﻿/*
Programas:
1. Escriba un programa que pregunte una y otra vez si desea continuar con el programa, 
siempre que se conteste que si, debe seguir ejecutando el programa, cuando conteste que no, 
debe terminar el programa

2. Escriba un programa que solicite una contraseña (el texto de la contraseña no es importante) 
y la vuelva a solicitar hasta que las dos contraseñas coincidan.

3. Escriba un programa que simule una alcancía. El programa solicitará primero una cantidad, 
que será la cantidad de dinero que queremos ahorrar. A continuación, el programa solicitará una y 
otra vez las cantidades que se irán ahorrando, hasta que el total ahorrado iguale o supere al 
objetivo. El programa no comprobará que las cantidades sean positivas.

4. Escriba un programa que mejore la simulación de la alcancía del ejercicio anterior, no 
permitiendo que se escriban cantidades negativas.

5. Escriba un programa que solicite una contraseña (el texto de la contraseña no es importante) y 
la vuelva a solicitar hasta que las dos contraseñas coincidan, con un límite de tres peticiones.
*/


using System;

namespace simple_while2
{
    class Program
    {
        static void Main(string[] args)
        {
            var target = 0.00;
            var quantity = 0.00;
            var total = 0.00;

            Console.WriteLine("Escribe la cantidad objetivo a ahorrar: ");
            target = Convert.ToDouble(Console.ReadLine());
            while (total < target)
            {
                Console.WriteLine("Digita la cantidad para ahorrar: ");
                quantity = Convert.ToDouble(Console.ReadLine());
                if (quantity > 0)
                {
                    total = total + quantity;
                    Console.WriteLine($"El total ahorrado es: {total}");
                }
                else
                {
                    Console.WriteLine("No se permite sacar dinero de la alcancia, hasta lograr el objetivo..");
                }
                
                
            }
            Console.WriteLine($"El Target de {target} a ahorrar, fue logrado con la cantidad {total}");
        }
    }
}
