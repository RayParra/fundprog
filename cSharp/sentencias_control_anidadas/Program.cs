﻿using System;

namespace sentencias_control_anidadas
{
    class Program
    {
        static void Main(string[] args)
        {
            var articulo = "Prohibido";
            var name = "Foo Bar";

            Console.WriteLine("Escribe tu nombre: ");
            name = Console.ReadLine();
            Console.WriteLine("Declara tus articulos: ");
            articulo = Console.ReadLine();

            if (name == "Foo Bar")
            {
                Console.WriteLine("Alerta, es el nombre buscado... ");
                if (articulo == "Prohibido")
                {
                    Console.WriteLine("Alerta de inspeccion, Posible Terrorista.. ");
                }
                else {
                    Console.WriteLine(" Sin alertas de inscpeccion.. ");
                }
            }
            else{
                Console.WriteLine("Sin alertas de Terrorista.. ");
            }
            //Console.WriteLine("Hello World!");
        }
    }
}
