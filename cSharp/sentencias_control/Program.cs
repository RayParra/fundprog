﻿using System;

namespace sentencias_control
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                Realizar un programa para determinar si el valor de la
                variable num1 es mayor al valor de la variable num2,
                escribir en pantalla el resultado
            */
            int num1, num2;
            
            Console.WriteLine("Escribe el valor para Num1: ");
            num1 = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Escribe el valor para Num2: ");
            num2 = Int32.Parse(Console.ReadLine());

            // Sentencias de control
            // IF - THEN - ELSE
            // num1 > num2 | num2 > num1  num1 < num2 | num1 = num2
            if (num1 > num2)
            {
                Console.WriteLine("El valor de Num1 es mayor que el valor de Num2");                
            }
            else if (num2 > num1){
                Console.WriteLine("El valor de Num2 es mayor que el valor de Num1");
            }
            else{
                Console.WriteLine("Los valores de Num1 y Num2 son iguales");
            }
        }
    }
}
