﻿/*
Programas:
1. Escriba un programa que pregunte una y otra vez si desea continuar con el programa, 
siempre que se conteste que si, debe seguir ejecutando el programa, cuando conteste que no, 
debe terminar el programa

2. Escriba un programa que solicite una contraseña (el texto de la contraseña no es importante) 
y la vuelva a solicitar hasta que las dos contraseñas coincidan.

3. Escriba un programa que simule una alcancía. El programa solicitará primero una cantidad, 
que será la cantidad de dinero que queremos ahorrar. A continuación, el programa solicitará una y 
otra vez las cantidades que se irán ahorrando, hasta que el total ahorrado iguale o supere al 
objetivo. El programa no comprobará que las cantidades sean positivas.

4. Escriba un programa que mejore la simulación de la alcancía del ejercicio anterior, no 
permitiendo que se escriban cantidades negativas.

5. Escriba un programa que solicite una contraseña (el texto de la contraseña no es importante) y 
la vuelva a solicitar hasta que las dos contraseñas coincidan, con un límite de tres peticiones.
*/

using System;

namespace simple_while3
{
    class Program
    {
        static void Main(string[] args)
        {
            var password1 = "";
            var password2 = "";
            var intent = 0;

            Console.WriteLine("Escribe tu contraseña: ");
            password1 = Console.ReadLine();
            while (password1 != password2 && intent < 3) 
            {
                 Console.WriteLine("Escribe de nuevo tu contraseña: ");
                 password2 = Console.ReadLine();
                 if (password1 == password2)
                 {
                  Console.WriteLine($"La contraseña {password2}, SI coincide con la contraseña Principal");   
                  Console.WriteLine("Acceso concedido!!");
                 }
                 else
                 {
                     Console.WriteLine($"La contraseña {password2}, NO coincide con la contraseña Principal");
                     intent ++;
                     Console.WriteLine($"Te quedan {3 - intent} intentos..");
                     Console.WriteLine("Intentos Agotados, por favor espere a ejecutar de nuevo... ");
                 }  
            }
            
    
            
            
        }
    }
}
