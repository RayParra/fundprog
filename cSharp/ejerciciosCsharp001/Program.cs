﻿using System;

namespace ejerciciosCsharp001
{
    class Program
    {
        static void Main(string[] args)
        {
            // 2x + x + 1 +3 // Expresion matematica
           //Variables
           int x;
           int resultado;

           // cuerpo del programa, aqui vamos a resolver la expresion matematica
           Console.WriteLine("Escribe el Valor de la variable x: ");
           x = Int32.Parse(Console.ReadLine());
           resultado =   x + 2 * x + 1 + 3;
           Console.WriteLine("El Resultado de la Expresion es: " + resultado);
        }
    }
}

