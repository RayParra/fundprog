﻿using System;

namespace matrices_bidimensionales
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] array2d = new int[2, 2];
            int [] collection = new int[3]{ 3, 4, 5};

           // Console.WriteLine("Hello World!");
           for (int row = 0; row < 2; row++)
           {
               for (int col = 0; col < 2; col++){
                   Console.WriteLine($"Row: {array2d[row, col]}");
               }
           }
           foreach (var item in array2d)
           {
               Console.WriteLine($"Items: {item}");
           }
           foreach (var item in collection)
           {
               Console.WriteLine($"{item}");
           }
        }
    }
}

/*
1, 2
3, 4
*/
