#codign: utf-8
#double-if.py
#@Ray Parra
"""
Programa para explicar como funcionan las sentencias de control no simples,
una sentencia de control permite al programador construir aplicaciones
en las cuales se puede controlar el flujo de informacion, es decir, las 
sentencias de control permiten tomar decisiones en los programas, la 
sintaxis base de estas sentencias es if - else
"""

#Ejemplos:

import random

numero_aleatorio = random.randint(1, 100)

if numero_aleatorio >= 70:
    print("El numero aleatorio {}, es un numero mayor o igual a 70".format(numero_aleatorio))
else:
    print("El numero aleatorio {}, es un numero menor a 70".format(numero_aleatorio))

dia_aleatorio = random.choice(['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'])

if dia_aleatorio == 'Lunes':
    print("Es inicio de Semana...")
else:
    print("No es inicio de Semana.")