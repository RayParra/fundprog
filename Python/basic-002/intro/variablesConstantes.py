#coding: utf-8
"""
Programa para explicar y comprender que es una variable y que es,
una constante. Como se declaran las variables y constantes en Python.
Como se utilizan las variables y las constantes  en Python
@ Ray Parra
#variablesConstantes.py
"""

# Variable:

# Constante:

## Declarar Variables y Constantes

a = 10
b = 34

numero = 45
flotante = 98.23

PI = 3.1416
CONSTANTE = "valor que no cambia durante la ejecucion"

variable_declarada = "Esto es una variable tipo string declarada"
var_string = "Como pueden observar no le especificamos el tipo de dato a la variable"
s = "Esto es porque Python tiene tipado dinamico"


## Como se utilizan las Variables y las Constantes
suma = a + b # ejemplo de como utilizar variables
print(suma) ## salida de una variable

# La asignacion de una variable o constante es una forma de utilizarlas
c = True


## la salida a pantalla de una variable es una forma de utilizarlas
print(variable_declarada)
print(var_string)
print(s)

#####################################################################################
# Para aprender mas acerca de este tema consultar el video "Variables y Constantes" en el canal
# de youtube, en el tutorial "Python: Libera tu Potencial"
#####################################################################################