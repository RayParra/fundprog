#coding: utf-8
"""
Programa que explica y muestra el funcionamiento de los diferentes tipos
de datos que puede utilizar Python, en este ejemplo veremos los tipos de 
datos básicos
@ Ray Parra
# tiposDatos.py
"""

# Tipos de datos Basicos
"""
En cualquier lenguaje de programación de alto nivel se manejan tipos de datos. 
Los tipos de datos definen un conjunto de valores que tienen una serie de 
características y propiedades determinadas.
En Python, todo valor que pueda ser asignado a una variable tiene asociado un tipo de dato.
En definitiva, un tipo de dato establece qué valores puede tomar una variable y qué operaciones 
se pueden realizar sobre la misma.

¿Qué tipos de datos trae consigo Python?

En Python podemos encontrar distintos tipos de datos con diferentes características y clasificaciones. 
En este tutorial repasaremos los tipos de datos básicos, aunque te introduciré otros tipos de datos 
que veremos en tutoriales posteriores.

Los tipos de datos básicos de Python son los booleanos, los numéricos (enteros, punto flotante y complejos) 
y las cadenas de caracteres.

"""

## Tipo Numericos
"""
Python define tres tipos de datos numéricos básicos: enteros, números de punto flotante (simularía el 
conjunto de los números reales, pero ya veremos que no es así del todo) y los números complejos.
"""
### Enteros
"""
El tipo de los números enteros es int. Este tipo de dato comprende el conjunto de todos los números 
enteros, pero como dicho conjunto es infinito, en Python el conjunto está limitado realmente por la 
capacidad de la memoria disponible. No hay un límite de representación impuesto por el lenguaje.

Pero tranquilidad, que para el 99'%' de los programas que desarrolles tendrás suficiente con el 
subconjunto que puedes representar.

Un número de tipo int se crea a partir de un literal que represente un número entero o bien como 
resultado de una expresión o una llamada a una función.

Ejemplos:
"""

### Flotantes
"""
Puedes usar el tipo float sin problemas para representar cualquier número real (siempre teniendo 
en cuenta que es una aproximación lo más precisa posible). Por tanto para longitudes, pesos, 
frecuencias, …, en los que prácticamente es lo mismo 3,3 que 3,300000000000000 el tipo float 
es el más apropiado.

Cuando un número float vaya a ser usado por una persona, en lugar de por el ordenador, puedes 
darle formato al número de la siguiente manera:
Ejemplos: 
"""

### Complejos
"""
El último tipo de dato numérico básico que tiene Python es el de los números complejos, complex.

Los números complejos tienen una parte real y otra imaginaria y cada una de ellas se representa 
como un float.

Para crear un número complejo, se sigue la siguiente estructura <parte_real>+<parte_imaginaria>j. 
Y se puede acceder a la parte real e imaginaria a través de los atributos real e imag
Ejemplos:
"""

## Tipo Booleandos
"""
En Python la clase que representa los valores booleanos es bool. Esta clase solo se puede instanciar 
con dos valores/objetos: True para representar verdadero y False para representar falso.

Una particularidad del lenguaje es que cualquier objeto puede ser usado en un contexto donde se requiera 
comprobar si algo es verdadero o falso. Por tanto, cualquier objeto se puede usar en la condición de un if 
o un while (son estructuras de control que veremos en temas posteriores) o como operando de una 
operación booleana.
Ejemplos:
"""

## Tipo String
"""
Una vez que hemos acabado con los números, es el turno de las letras 😜

Otro tipo básico de Python, e imprescindible, son las secuencias o cadenas de caracteres. Este tipo es 
conocido como string aunque su clase verdadera es str.

Formalmente, un string es una secuencia inmutable de caracteres en formato Unicode.

Para crear un string, simplemente tienes que encerrar entre comillas simples '' o dobles "" una secuencia 
de caracteres.

Se puede usar indistintamente comillas simples o dobles, con una particularidad. Si en la cadena de 
caracteres se necesita usar una comilla simple, tienes dos opciones: usar comillas dobles para encerrar 
el string, o bien, usar comillas simples pero anteponer el carácter \ a la comilla simple del interior de 
la cadena. El caso contrario es similar.

Ejemplos:
"""

## Otro tipos de datos en Python
"""
Hasta aquí hemos repasado los tipos de datos básicos de Python, sin embargo, el lenguaje ofrece muchos tipos 
más. Te hago aquí un avance de los más importantes, aunque los veremos en detalle en otros tutoriales.

Además de los tipos básicos, otros tipos fundamentales de Python son las secuencias (list y tuple), los 
conjuntos (set) y los mapas (dict).

Todos ellos son tipos compuestos y se utilizan para agrupar juntos varios valores.

Las listas son secuencias mutables de valores.
Las tuplas son secuencias inmutables de valores.
Los conjuntos se utilizan para representar conjuntos únicos de elementos, es decir, en un conjunto no pueden 
existir dos objetos iguales.
Los diccionarios son tipos especiales de contenedores en los que se puede acceder a sus elementos a partir de 
una clave única.
Algunos Ejemplos:(En temas posteriores abarcaremos mas ejemplos)
"""

## Tipo de dato de una variable
"""
Ahora te voy a presentar dos funciones para que puedas jugar con todo lo que hemos visto en este tutorial. 
Son type() e isinstance():

type() recibe como parámetro un objeto y te devuelve el tipo del mismo.
isinstance() recibe dos parámetros: un objeto y un tipo. Devuelve True si el objeto es del tipo que se pasa 
como parámetro y False en caso contrario.
Ejemplos:
"""

## Convertir a otro tipo de dato
"""
Lo último que veremos en este tutorial sobre tipos de datos es la conversión de tipos.

¿Esto qué significa?

Imagina que tienes una variable edad de tipo string cuyo valor es '25'. Se podría decir que edad, aunque 
realmente es una cadena de caracteres, contiene un número. Sin embargo, si intentas sumar 10 a edad, el 
intérprete te dará un error porque edad es de tipo str y 10 un tipo numérico.
Ejemplos:
"""


#####################################################################################
# Para aprender mas acerca de este tema consultar el video "tiposDatos" en el canal
# de youtube, en el tutorial "Python: Libera tu Potencial"
#####################################################################################