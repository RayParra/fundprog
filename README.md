# FundProg

Repositorio para las practicas de la materia de fundamentos de programación. Todos los programas y proyectos utilizados en la materia estarán aquí.


Para un mejor flujo de aprendizaje en la materia se recomienda utilizar el metodo aprendizaje basado en proyectos, proveniente del metodo flipped classroom, el cual de manera general consiste en aprendizaje activo por parte del estudiante y aprendizaje pasivo por parte del docente.

En una clase regular el docente tiene la participacion activa en el aprendizaje, en el metodo flipped classroom este rol se invierte, el docente se convierte en guia lo que le permite al estudiante tomar un rol mas activo en el aprendizaje. 

Todas las clases, practicas y cualquier otro producto, formaran parte o estaran relacionadas al proyecto de la clase, desde el primer dia de clases se formaran los equipos de trabajo y se propone el proyecto de la materia.

Flujo de trabajo: 
- En la materia se realizaran practicas en 3 lenguajes de programacion(C#, Rust, Python), con el objetivo de maximizar el alcance y potencial de todos los estudiantes. 
- Desde la primera clase los estudiantes deberan generar un repositorio en gitlab para agregar todas las practicas, ejemplos, codigos, programas y proyectos utilizados en la materia. Ademas tendran disponible el repositorio de la clase: "https://gitlab.com/RayParra/fundprog"
- Todas las tareas estaran publicadas en el classroom, por lo que el estudiante tendra que investigar, leer y comprender los temas que se indiquen en las actividades, con el objetivo de utilizar el tiempo de la clase en practicas, aclaracion de dudas y resolucion de problemas.
- El docente proporcionara desde el primer dia de clases el metodo o forma de evaluacion, el cual consiste en un sistema de puntaje minimo requerido para generar el derecho a evaluacion del proyecto. Lo cual indica que la calificacion del proyecto representa el 100% de la calificacion de la materia.
- El proyecto constara de 3 a 5 avances programados, por lo cual es importante atender cada uno de los avances, debido a que cada avance representa una parte de la calificacion final.
- En el classroom, se encuentra una seccion para las tareas, clasificadas por unidades y categorizadas en tareas, presentaciones, laboratorios y proyectos. Asi mismo, tambien cuenta con una seccion para los laboratorios, una seccion para el proyecto y una seccion README, la cual es importante leer antes de iniciar la clase, en esta seccion se encuentra informacion complementaria para el inicio de la materia.

Recursos:
- En el folder "src", se encuentran recursos disponibles para mejorar la experiencia de aprendizaje en la materia. Los recursos pueden ser desde libros electronicos, textos, practicas, video tutoriales, imagenes, infografias, etc.
- El canal de YouTube "DinamycDuo"(se recomienda suscribirse al canal) contiene video tutoriales que apoyan los temas de las clases, este canal se mantendra actualizado en temas, practicas y demas objetos de aprendizaje, con el objetivo de mejorar la experiencia de aprendizaje.
- El repositorio de gitlab, tiene el objetivo de mantener actualizados los temas, recursos y practicas de la clase.
